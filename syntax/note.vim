syn match Comment /^#.*/ 
syn match Comment "^//.*" contains=Keyword
syn match Comment /^\^.*/
syn match Keyword "<[^<>]*>"
syn match Keyword "\[[^\[\]]*\]"
syn match IfStatement /^?__.*/
syn match Timestamp /^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d:/

hi IfStatement ctermfg=lightred
hi Timestamp ctermbg=green ctermfg=black
