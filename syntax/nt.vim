syn match Comment /^#.*/ 
syn match Comment "^//.*"
syn match Comment /^\^.*/
syn match Timestamp /^\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d:/
syn match Waiting /WAITING:.*/
syn match File /\[[^X ].*\]/
syn match Strikethrough /\[X\].*$/

hi Timestamp ctermbg=green ctermfg=black
hi Waiting ctermfg=red
hi Strikethrough cterm=strikethrough
hi File ctermfg=brown
