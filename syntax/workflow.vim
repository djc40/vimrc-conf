syn match Comment /^#.*/ 
syn match Comment "^//.*" contains=Keyword
syn match Comment /^\*\*.*/
syn match Keyword "<[^<>]*>"
syn match Keyword "\[[^\[\]]*\]"
