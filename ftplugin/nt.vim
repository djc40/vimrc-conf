set tabstop=4
set shiftwidth=4
set breakindent
set breakindentopt=shift:2,list:2
set comments=b:-,b:\[\ \]
let &formatlistpat='^\s*\[[ X]\]'
nnoremap <S-K> "fyi[:e<space><c-r>f<CR>

