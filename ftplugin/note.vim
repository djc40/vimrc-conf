nnoremap <leader>l ma0d$:r !sed -n '/^<c-r>"/, /}}}/p' ~/pentest_notes/workflow<CR>dd'a2dd
set fdm=manual
nnoremap <F2> zzI<c-r>=strftime("%F %R:%S")<CR>: <ESC>jo
inoremap <F2> <ESC>zzI<c-r>=strftime("%F %R:%S")<CR>: <ESC>jo
nnoremap <F3> zzI<c-r>=strftime("%F %R:%S")<CR>: <ESC>o
inoremap <F3> <ESC>zzI<c-r>=strftime("%F %R:%S")<CR>: <ESC>o
nnoremap <F4> zzI<c-r>=strftime("%F %R:%S")<CR>: 
inoremap <F4> <ESC>zzI<c-r>=strftime("%F %R:%S")<CR>: 
