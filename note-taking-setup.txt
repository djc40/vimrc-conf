run ./daily.sh every time you want to repopulate your list files (probably daily)

I keep a daily log file in the format YYYY-MM-DD.nt. That file usually is broken into a few sections such as:
# Plan

# Notes (and maybe a few other random note type sections)

# Worklog

In #Plan I put the things I want to focus on for the day, other middle sections have notes that maybe dont have enough to warrant their own note file. And #Worklog I update thoughout the day with what I actually got accomplished

Then there's the project files. All project files follow the format pr-<NAME>.nt. Originally I was keeping all my todos in one list but that did not scale well. Additionally, it got annoying have the information I needed separated from the tasks I wanted to do. So following some advice from GTD there, I have a file called pr-oneoffs.nt where I'll put tasks. However, if a task is going to have multiple related parts, I create a file called pr-<PROJECT_NAME>.nt and put all the tasks related to that project in there. When you run daily.sh it goes through all the project files and puts all those tasks in the todo.nt file. That way you can quickly look at all the todos for each project and easily know which file they are in. Then you can put your cursor over the file and click <CTRL-K> and it will open that file. Don't check off tasks in the todo.nt file, check them off in the related project file.

If you have a task that is blocked you can add WAITING: and a date and then the next time you run daily.sh it will put those tasks at the top of the todo.nt so you can keep track if you need follow up.

For meetings I'll create a mt-<NAME>.nt file and for notes where there might be a lot of stuff I'll put that in a nt-<NAME>.nt file. The great thing is you can put the name of these meetings and notes into the project file that they are associated with and easily navigate to them with <Ctrl-K>. You could also probably extend this easily.

From there the projects.nt, meetings.nt and notes.nt files all get populated with all project, meetings, and notes files in the current directory when you run daily. This makes it easy to browse through meeting or notes files if you can't remember exactly what youre looking for.

Additionally, you can use vimgrep with a :vim /search-term/ * to search over all the files and then do :copen to see matches.

So then the work flow becomes something like this. Run daily.sh and create a new daily log (could probably add this to daily.sh lol) which would be something like 2025-01-01.nt. Then I'd do a :vsp todo.nt. I'd look over all the tasks I have for all the projects and any thing that is in waiting and then copy over the most important ones into my dailylog so I can keep track of what I want to do today. From there I'd work on those tasks, taking notes in the associated project file or if theres a ton of info maybe making a new note file and putting the name of that in the project file. As I knock stuff out I'll check off tasks in the original project files as well as add things to the worklog. A lot of times you end up doing stuff that you didn't plan so still good to put that info in the worklog even if you dont wanna make a new task just to check it off.

This creates a system where you can easily see what you've accomplished each day, easily see what you have already done for a project and what other tasks you need to do or are in a blocked state. And the information that is related to that project is easily found from that project.
