"source $VIMRUNTIME/defaults.vim

set nocompatible
filetype off
filetype plugin indent on
syntax enable
set belloff=all
set mouse=
set hidden
set nobackup
set ignorecase
set hlsearch
set laststatus=2
set completeopt=longest,menuone
set wildmode=list:longest,full
set display=lastline
nnoremap <leader><space> :nohlsearch<CR>
nnoremap <space> za
nnoremap <c-h> <c-w>h
nnoremap <c-j> <c-w>j
nnoremap <c-k> <c-w>k
nnoremap <c-l> <c-w>l
nnoremap n nzz
nnoremap N Nzz
nnoremap { { zz
nnoremap } } zz
