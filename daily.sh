#!/bin/bash

# generated lists; dont edit these files directly
cat /dev/null > todo.nt
cat /dev/null > projects.nt
cat /dev/null > meetings.nt
cat /dev/null > notes.nt
cat /dev/null > dailylogs.nt

# waiting list in todos
echo "#waiting" >> todo.nt
for i in pr-*.nt; do
	if grep -q "WAITING" $i; then
		echo "[$i]" >> todo.nt;
		grep "\[ \]" $i | grep "WAITING" >> todo.nt;
		echo >> todo.nt;
	fi
done
echo >> todo.nt

# task list in todos
echo "#todo" >> todo.nt
for i in pr-*.nt; do
	if grep -q "\[ \]" $i; then
		echo "[$i]" >> todo.nt;
		grep "\[ \]" $i | grep -v "WAITING" >> todo.nt;
		echo >> todo.nt;
	fi
done

# populate projects list
for i in pr-*.nt; do
	echo "[$i]" >> projects.nt
done

# populate meetings list
for i in mt-*.nt; do
	echo "[$i]" >> meetings.nt
done

# populate notes list
for i in nt-*.nt; do
	echo "[$i]" >> notes.nt
done

# populate dailylog list
for i in $(ls | grep -E "\d\d\d\d-\d\d-\d\d.nt"); do
	echo "[$i]" >> dailylogs.nt
done
